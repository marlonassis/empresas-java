# Criação do banco de dados

CREATE DATABASE db_empresa_java
    WITH 
    OWNER = postgres
    ENCODING = 'LATIN1'
    LC_COLLATE = 'C'
    LC_CTYPE = 'C'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;
    template = template0

# Exemplos de URL para consumo da API

## Login

- **Ação**: Logar
- Url: http://127.0.0.1:8080/auth
- Method: POST
- Corpo:
```json
{
    "usuario": "jorge",
    "senha": "123"
}
```

## Usuário

- **Ação**: Cadastrar
- Url: http://127.0.0.1:8080/api/cadastrar-usuario
- Método: POST
- Corpo:
```json
{
    "nome": "Nome1",
    "perfil": "ROLE_ADMIN",
    "status": "A",
    "login": "usuario1",
    "senha": "123"
}
```

- **Ação**: Consultar (Considerei a edição como sendo a consulta e atualização do usuário)
- Url: Url: http://127.0.0.1:8080/api/usuario/ba4f181e-5527-49a4-aff6-8473f0d50aa4
- Método: DELETE

.

- **Ação**: Atualizar
- Url: http://127.0.0.1:8080/api/usuario
- Método: PUT
- Corpo:
```json
{
    "uuid": "ba4f181e-5527-49a4-aff6-8473f0d50aa4",
    "nome": "Nome2",
    "perfil": "ADM",
    "status": "I"
}
```

- **Ação**: Deletar
- Url: Url: http://127.0.0.1:8080/api/usuario/ba4f181e-5527-49a4-aff6-8473f0d50aa4
- Método: DELETE

.

- **Ação**: Listar usuários não administradores e ativos
- Url: http://127.0.0.1:8080/api/usuario/listagem/?pag=0
- Método: GET


## Filme

- **Ação**: Cadastrar
- Url: http://127.0.0.1:8080/api/filme/
- Método: POST
- Corpo:
```json
{
	"nome": "Independence Day",
	"nomeDiretor": "Roland Emmerich",
	"atores": [{"nome": "Will Smith"}, {"nome": "Bill Pullman"}, {"nome": "Jeff Goldblum"}],
	"sinopse": "Conta a história de uma hipotética invasão alienígena à Terra, focando um grupo heterogêneo de pessoas no deserto de Nevada que, junto com o resto da população mundial, participa da última chance de contra-ataque da humanidade em 4 de julho - mesma data do feriado do Dia da Independência dos Estados Unidos.",
	"genero": "Ficção Científica"
}

{
	"nome": "Pulp Fiction",
	"nomeDiretor": "Quentin Tarantino",
	"atores": [{"nome": "John Travolta"}, {"nome": "Samuel L. Jackson"}, {"nome": "Uma Thurman"}],
	"sinopse": "Dirigido de uma forma altamente estilizada, Pulp Fiction narra três histórias diferentes, todavia entrelaçadas, sobre dois assassinos profissionais, o gângster que os chefia e sua esposa, um pugilista pago para perder uma luta e um casal assaltando um restaurante, em Los Angeles na década de 1990. Um tempo considerável do filme é destinado a conversas e monólogos que revelam as perspectivas de vida e o senso de humor das personagens.",
	"genero": "Drama Policial"
}

{
	"nome": "Goodfellas",
	"nomeDiretor": "Martin Scorsese",
	"atores": [{"nome": "Ray Liotta"}, {"nome": "Robert De Niro"}, {"nome": "Joe Pesci"}],
	"sinopse": "Contada em primeira pessoa pelo ítalo-irlandês Henry Hill (Ray Liotta), o filme traça uma biografia da Máfia. Ainda jovem, Henry se envolve com Tommy DeVito (Joe Pesci) e James Conway (Robert De Niro) e se casa com Karen (Lorraine Bracco), jovem judia que vê toda a sua vida social se misturando com o crime.",
	"genero": "Drama Policial"
}
```

- **Ação**: Listar filmes por meio de filtros
- Url: http://127.0.0.1:8080/api/filme/listagem?diretor=Martin Scorsese&nome=Goodfellas&genero=Comédia&ator=Will Smith
- Método: GET

.

- **Ação**: Votar em um filme
- Url: http://127.0.0.1:8080/api/filme/votar/
- Método: POST
- Corpo:
```json
{
    "uuid": "73570533-eddb-464b-89f6-c86945527f73",
    "voto": "1"
}
```

- **Ação**: Consultar detalhes de um filme
- Url: http://127.0.0.1:8080/api/filme/73570533-eddb-464b-89f6-c86945527f73
- Método: GET