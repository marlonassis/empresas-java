package br.com.empresasjava.api.repositories;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Optional;
import java.util.UUID;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.ActiveProfiles;

import br.com.empresasjava.api.entities.Usuario;
import br.com.empresasjava.api.enums.PerfilEnum;
import br.com.empresasjava.api.enums.StatusUsuarioEnum;
import br.com.empresasjava.api.utils.PasswordUtils;

@SpringBootTest
@ActiveProfiles("test")
public class UsuarioRepositoryTest {
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	private UUID uuid;
	private final String NOME_USUARIO = "usuario1";
	private final String SENHA = "123456";
	private final BCryptPasswordEncoder bcpe = new BCryptPasswordEncoder();
	
	@BeforeEach
	public void setUp() throws Exception {
		Usuario usuarioExemplo = getUsuarioExemplo();
		this.usuarioRepository.save(usuarioExemplo);
		this.uuid = usuarioExemplo.getUuid();
	}
	
	@AfterEach
	public final void tearDown() {
		this.usuarioRepository.deleteAll();
	}
	
	@Test
	public void testBuscarId() {
		Optional<Usuario> usuario = this.usuarioRepository.findById(this.uuid);
		assertEquals(this.uuid, usuario.get().getUuid());
	}
	
	@Test
	public void testSqlInjection() {
		Optional<Usuario> usuario = this.usuarioRepository.findByLogin(this.NOME_USUARIO);
		assertFalse(bcpe.matches(this.SENHA + " OR 1=1", usuario.get().getSenha()));
	}
	
	@Test
	public void testBuscarUsuarioSucesso() {
		Optional<Usuario> usuario = this.usuarioRepository.findByLogin(this.NOME_USUARIO);
		assertEquals(this.uuid, usuario.get().getUuid());
	}
	
	@Test
	public void testBuscarUsuarioFalha() {
		Optional<Usuario> usuario = this.usuarioRepository.findByLogin("abc");
		assertFalse(usuario.isPresent());
	}
	
	@Test
	public void testUsuarioSenhaSucesso() {
		Optional<Usuario> usuario = this.usuarioRepository.findByLogin(this.NOME_USUARIO);
		assertTrue(bcpe.matches(this.SENHA, usuario.get().getSenha()));
	}
	
	@Test
	public void testUsuarioSenhaFalha() {
		Optional<Usuario> usuario = this.usuarioRepository.findByLogin(this.NOME_USUARIO);
		assertFalse(bcpe.matches("123", usuario.get().getSenha()));
	}
	
	private Usuario getUsuarioExemplo() {
		Usuario usuario = new Usuario();
		usuario.setLogin(NOME_USUARIO);
		usuario.setNome("Nome1");
		usuario.setPerfil(PerfilEnum.ROLE_ADMIN);
		usuario.setSenha(PasswordUtils.gerarBCrypt(SENHA));
		usuario.setStatus(StatusUsuarioEnum.A);
		return usuario;
	}
	
}
