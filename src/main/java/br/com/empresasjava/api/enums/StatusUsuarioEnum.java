package br.com.empresasjava.api.enums;

public enum StatusUsuarioEnum {
	A("Ativo"),
	I("Inativo");
	
	StatusUsuarioEnum(String label){
		this.label = label;
	}
	
	private String label;
	
	public String getLabel(){
		return label;
	}
	
	public void setLabel(String label){
		this.label = label;
	}

}
