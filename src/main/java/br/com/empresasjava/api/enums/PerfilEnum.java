package br.com.empresasjava.api.enums;

public enum PerfilEnum {
	ROLE_ADMIN("Administrador"),
	ROLE_USUARIO("Usuário");
	
	PerfilEnum(String label){
		this.label = label;
	}
	
	private String label;
	
	public String getLabel(){
		return label;
	}
	
	public void setLabel(String label){
		this.label = label;
	}
	
}
