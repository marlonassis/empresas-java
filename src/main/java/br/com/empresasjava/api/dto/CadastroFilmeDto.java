package br.com.empresasjava.api.dto;

import java.util.List;
import java.util.UUID;

import javax.validation.constraints.NotEmpty;

public class CadastroFilmeDto {
	
	private UUID uuid;
	private String nome;
	private String nomeDiretor;
	private List<CadastroAtorDto> atores;
	private String sinopse;
	private String genero;
	private Double votoMedia;
	
	public UUID getUuid() {
		return uuid;
	}
	
	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}
	
	@NotEmpty(message="Nome não pode ser vazio.")
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@NotEmpty(message="Nome do diretor não pode ser vazio.")
	public String getNomeDiretor() {
		return nomeDiretor;
	}
	
	public void setNomeDiretor(String nomeDiretor) {
		this.nomeDiretor = nomeDiretor;
	}
	
	@NotEmpty(message="Atores não pode ser vazio.")
	public List<CadastroAtorDto> getAtores() {
		return atores;
	}
	
	public void setAtores(List<CadastroAtorDto> atores) {
		this.atores = atores;
	}
	
	@NotEmpty(message="Sinopse não pode ser vazio.")
	public String getSinopse() {
		return sinopse;
	}
	
	public void setSinopse(String sinopse) {
		this.sinopse = sinopse;
	}
	
	@NotEmpty(message="Gênero não pode ser vazio.")
	public String getGenero() {
		return genero;
	}
	
	public void setGenero(String genero) {
		this.genero = genero;
	}
	
	public Double getVotoMedia() {
		return votoMedia;
	}
	
	public void setVotoMedia(Double votoMedia) {
		this.votoMedia = votoMedia;
	}
}
