package br.com.empresasjava.api.dto;

import java.util.UUID;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;

public class CadastroVotoDto {
	
	private UUID uuid;
	private Integer voto;
	
	public UUID getUuid() {
		return uuid;
	}
	
	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}
	
	@NotNull(message="Voto não pode ser vazio.")
	@Range(min = 0, max = 4, message = "O voto deve ser entre 0 e 4 estrelas.")
	public Integer getVoto() {
		return voto;
	}
	
	public void setVoto(Integer voto) {
		this.voto = voto;
	}
	
}
