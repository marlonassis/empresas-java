package br.com.empresasjava.api.dto;

import java.util.UUID;

import javax.validation.constraints.NotEmpty;

public class CadastroAtorDto {
	
	private UUID uuid;
	private String nome;
	
	public UUID getUuid() {
		return uuid;
	}
	
	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}
	
	@NotEmpty(message="Nome do ator não pode ser vazio.")
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
		
}
