package br.com.empresasjava.api.dto;

import java.util.UUID;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import br.com.empresasjava.api.enums.PerfilEnum;
import br.com.empresasjava.api.enums.StatusUsuarioEnum;

public class AtualizacaoUsuarioDto {
	private UUID uuid;
	private String nome;
	private PerfilEnum perfil;
	private StatusUsuarioEnum status;
	
	public UUID getUuid() {
		return uuid;
	}
	
	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}
	
	@NotEmpty(message="Nome não pode ser vazio.")
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Enumerated(EnumType.STRING)
	@NotNull(message="Perfil não pode ser vazio.")
	public PerfilEnum getPerfil() {
		return perfil;
	}
	
	public void setPerfil(PerfilEnum perfil) {
		this.perfil = perfil;
	}
	
	@Enumerated(EnumType.STRING)
	@NotNull(message="Status não pode ser vazio.")
	public StatusUsuarioEnum getStatus() {
		return status;
	}
	
	public void setStatus(StatusUsuarioEnum status) {
		this.status = status;
	}
	
}
