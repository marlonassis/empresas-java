package br.com.empresasjava.api.repositories;

import java.util.Collection;
import java.util.UUID;

import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import br.com.empresasjava.api.entities.Filme;

@NamedQueries(
		{@NamedQuery(name = "FilmeRepository.findByDiretorOrNomeOrGenero", 
					 query = "SELECT DISTINCT o FROM Filme o "
					 		+ "LEFT JOIN o.atores a "
					 		+ "WHERE o.nomeDiretor = :nomeDiretor OR "
					 		+ "o.nome = :nome OR "
					 		+ "o.genero = :genero OR "
					 		+ "a.nome IN :atores"),}
)
public interface FilmeRepository extends JpaRepository<Filme, UUID> {

	@Transactional(readOnly = true)
	Page<Filme> findByNomeDiretorOrNomeOrGeneroOrAtoresNomeIn(@Param("nomeDiretor") String diretor, 
												@Param("nome") String nome, 
												@Param("genero") String genero, 
												@Param("atores") Collection<String> atores, Pageable pageable);
	
}
