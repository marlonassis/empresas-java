package br.com.empresasjava.api.repositories;

import java.util.Optional;
import java.util.UUID;

import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import br.com.empresasjava.api.entities.Usuario;
import br.com.empresasjava.api.enums.PerfilEnum;
import br.com.empresasjava.api.enums.StatusUsuarioEnum;

@NamedQueries(
		{@NamedQuery(name = "UsuarioRepository.findByPerfilAndStatus", 
					 query = "SELECT o FROM Usuario o WHERE o.perfil = :perfil AND o.status = :status"),}
)
public interface UsuarioRepository extends JpaRepository<Usuario, UUID>{

	@Transactional(readOnly = true)
	Optional<Usuario> findByLogin(String login);
	
	@Transactional(readOnly = true)
	Page<Usuario> findByPerfilAndStatus(@Param("perfil") PerfilEnum perfil, @Param("status") StatusUsuarioEnum status, Pageable pageable);
	
}
