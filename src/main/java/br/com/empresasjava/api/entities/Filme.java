package br.com.empresasjava.api.entities;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="tb_filme")
public class Filme implements Serializable {
		
	/**
	 * 
	 */
	private static final long serialVersionUID = 893210740717143044L;
	
	private UUID uuid;
	private String nome;
	private String nomeDiretor;
	private List<Ator> atores;
	private String sinopse;
	private String genero;
	private double votoMedia;
	private int votoTotal;
	private int votoQuantidade;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "uuid_filme", updatable = false, unique = true, nullable = false)
	public UUID getUuid() {
		return uuid;
	}
	
	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}
	
	@Column(name = "cv_nome", nullable = false)
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	@Column(name = "cv_nome_diretor", nullable = false)
	public String getNomeDiretor() {
		return nomeDiretor;
	}
	
	public void setNomeDiretor(String nomeDiretor) {
		this.nomeDiretor = nomeDiretor;
	}
	
	@OneToMany(mappedBy = "filme", fetch = FetchType.LAZY, cascade = {CascadeType.PERSIST})
	public List<Ator> getAtores() {
		return atores;
	}
	
	public void setAtores(List<Ator> atores) {
		this.atores = atores;
	}
	
	@Column(name = "cv_sinopse", nullable = false)
	public String getSinopse() {
		return sinopse;
	}
	
	public void setSinopse(String sinopse) {
		this.sinopse = sinopse;
	}
	
	@Column(name = "cv_genero", nullable = false)
	public String getGenero() {
		return genero;
	}
	
	public void setGenero(String genero) {
		this.genero = genero;
	}
	
	@Column(name = "fl_voto_media", nullable = false)
	public double getVotoMedia() {
		return votoMedia;
	}
	
	public void setVotoMedia(double votoMedia) {
		this.votoMedia = votoMedia;
	}
	
	@Column(name = "in_voto_total", nullable = false)
	public int getVotoTotal() {
		return votoTotal;
	}
	
	public void setVotoTotal(int votoTotal) {
		this.votoTotal = votoTotal;
	}
	
	@Column(name = "in_voto_quantidade", nullable = false)
	public int getVotoQuantidade() {
		return votoQuantidade;
	}
	
	public void setVotoQuantidade(int votoQuantidade) {
		this.votoQuantidade = votoQuantidade;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((genero == null) ? 0 : genero.hashCode());
		result = prime * result + ((nome == null) ? 0 : nome.hashCode());
		result = prime * result + ((nomeDiretor == null) ? 0 : nomeDiretor.hashCode());
		result = prime * result + ((sinopse == null) ? 0 : sinopse.hashCode());
		result = prime * result + ((uuid == null) ? 0 : uuid.hashCode());
		long temp;
		temp = Double.doubleToLongBits(votoMedia);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + votoQuantidade;
		result = prime * result + votoTotal;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Filme other = (Filme) obj;
		if (genero == null) {
			if (other.genero != null)
				return false;
		} else if (!genero.equals(other.genero))
			return false;
		if (nome == null) {
			if (other.nome != null)
				return false;
		} else if (!nome.equals(other.nome))
			return false;
		if (nomeDiretor == null) {
			if (other.nomeDiretor != null)
				return false;
		} else if (!nomeDiretor.equals(other.nomeDiretor))
			return false;
		if (sinopse == null) {
			if (other.sinopse != null)
				return false;
		} else if (!sinopse.equals(other.sinopse))
			return false;
		if (uuid == null) {
			if (other.uuid != null)
				return false;
		} else if (!uuid.equals(other.uuid))
			return false;
		if (Double.doubleToLongBits(votoMedia) != Double.doubleToLongBits(other.votoMedia))
			return false;
		if (votoQuantidade != other.votoQuantidade)
			return false;
		if (votoTotal != other.votoTotal)
			return false;
		return true;
	}
	
}
