package br.com.empresasjava.api.security;

import java.util.Collection;
import java.util.UUID;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import br.com.empresasjava.api.enums.PerfilEnum;

public class JwtUser implements UserDetails {

	private static final long serialVersionUID = 6447489586436746537L;
	
	private UUID id;
	private String username;
	private String password;
	private PerfilEnum perfil;
	private Collection<? extends GrantedAuthority> authorities;

	public JwtUser(UUID id, String username, String password, PerfilEnum perfil, Collection<? extends GrantedAuthority> authorities) {
		this.id = id;
		this.username = username;
		this.password = password;
		this.perfil = perfil;
		this.authorities = authorities;
	}

	public UUID getId() {
		return id;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return authorities;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public PerfilEnum getPerfil() {
		return this.perfil;
	}
	
}
