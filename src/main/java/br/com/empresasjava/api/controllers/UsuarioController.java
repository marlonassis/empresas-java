package br.com.empresasjava.api.controllers;

import java.security.NoSuchAlgorithmException;
import java.util.Optional;
import java.util.UUID;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.empresasjava.api.dto.AtualizacaoUsuarioDto;
import br.com.empresasjava.api.dto.CadastroUsuarioDto;
import br.com.empresasjava.api.entities.Usuario;
import br.com.empresasjava.api.enums.PerfilEnum;
import br.com.empresasjava.api.enums.StatusUsuarioEnum;
import br.com.empresasjava.api.response.Response;
import br.com.empresasjava.api.services.UsuarioService;

@RestController
@RequestMapping("/api/usuario")
@CrossOrigin(origins = "127.0.0.1")
public class UsuarioController {
	
	private static final Logger log = LoggerFactory.getLogger(UsuarioController.class);

	@Autowired
	private UsuarioService usuarioService;
	
	@Value("${paginacao.qtd_por_pagina}")
	private int qtdPorPagina;
	
	/**
	 * Atualiza um usuário.
	 * 
	 * @param atualizacaoUsuarioDto
	 * @param result
	 * @return ResponseEntity<Response<AtualizacaoUsuarioDto>>
	 * @throws NoSuchAlgorithmException
	 */
	@PutMapping
	public ResponseEntity<Response<AtualizacaoUsuarioDto>> atualizar(@Valid @RequestBody AtualizacaoUsuarioDto atualizacaoUsuarioDto, BindingResult result) {
		log.info("Atualizando Usuário: {}", atualizacaoUsuarioDto.toString());
		Response<AtualizacaoUsuarioDto> response = new Response<AtualizacaoUsuarioDto>();
		
		if (result.hasErrors()) {
			log.error("Erro validando dados de atualização usuário: {}", result.getAllErrors());
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.badRequest().body(response);
		}
		
		Optional<Usuario> usuarioBanco = usuarioService.usuarioPorId(atualizacaoUsuarioDto.getUuid());
		if(!usuarioBanco.isPresent()) {
			log.error("Usuário não encontrado: {}", atualizacaoUsuarioDto.getUuid());
			return ResponseEntity.notFound().build();
		}
		
		Usuario usuario = this.converterDtoParaUsuarioAtualizacao(atualizacaoUsuarioDto, usuarioBanco.get());
		
		this.usuarioService.persistir(usuario);

		response.setData(atualizacaoUsuarioDto);
		return ResponseEntity.ok(response);
	}
	
	/**
	 * Converte os dados do DTO para usuário.
	 * 
	 * @param cadastroPJDto
	 * @return Usuario
	 */
	private Usuario converterDtoParaUsuarioAtualizacao(AtualizacaoUsuarioDto dto, Usuario usuario) {
		usuario.setNome(dto.getNome());
		usuario.setPerfil(dto.getPerfil());
		usuario.setStatus(dto.getStatus());

		return usuario;
	}
	
	/**
	 * Exclui logicamente um usuário.
	 * 
	 * @param id
	 * @return ResponseEntity<Response<Lancamento>>
	 */
	@DeleteMapping(value = "/{id}")
	public ResponseEntity<Response<String>> remover(@PathVariable("id") UUID id) {
		log.info("Removendo lançamento: {}", id);
		Response<String> response = new Response<String>();
		Optional<Usuario> usuario = this.usuarioService.usuarioPorId(id);

		if (!usuario.isPresent()) {
			log.info("Erro ao remover devido ao usuario ID: {} ser inválido.", id);
			response.getErrors().add("Erro ao deletar o usuário. Registro não encontrado para o id " + id);
			return ResponseEntity.badRequest().body(response);
		}
		
		Usuario usuarioDelecao = usuario.get();
		usuarioDelecao.setStatus(StatusUsuarioEnum.I);
		this.usuarioService.persistir(usuarioDelecao);
		return ResponseEntity.ok(new Response<String>());
	}
	
	/**
	 * Consulta um usuário.
	 * 
	 * @param id
	 * @return ResponseEntity<Response<Lancamento>>
	 */
	@GetMapping(value = "/{id}")
	public ResponseEntity<Response<CadastroUsuarioDto>> consultar(@PathVariable("id") UUID id) {
		log.info("Consultando usuário: {}", id);
		Response<CadastroUsuarioDto> response = new Response<CadastroUsuarioDto>();
		Optional<Usuario> usuario = this.usuarioService.usuarioPorId(id);

		if (!usuario.isPresent()) {
			log.error("Erro validando dados de cadastro usuário: {}", id);
			return ResponseEntity.notFound().build();
		}
		
		response.setData(this.converterCadastroUsuarioDto(usuario.get()));
		return ResponseEntity.ok(response);
	}
	
	/**
	 * Popula o DTO de cadastro com os dados do usuário.
	 * 
	 * @param usuario
	 * @return UsuarioPJDto
	 */
	private CadastroUsuarioDto converterCadastroUsuarioDto(Usuario usuario) {
		CadastroUsuarioDto dto = new CadastroUsuarioDto();
		dto.setUuid(usuario.getUuid());
		dto.setNome(usuario.getNome());
		dto.setPerfil(usuario.getPerfil());
		dto.setStatus(usuario.getStatus());
		dto.setLogin(usuario.getLogin());
		
		return dto;
	}
	
	/**
	 * Retorna a listagem de usuários não administradores e ativos.
	 * 
	 * @return ResponseEntity<Response<CadastroUsuarioDto>>
	 */
	@GetMapping(value = "/listagem")
	public ResponseEntity<Response<Page<CadastroUsuarioDto>>> listarUsuariosNaoAdministradoresAtivos(
			@RequestParam(value = "pag", defaultValue = "0") int pag) {
		log.info("Buscando usuários na página: {}", pag);
		Response<Page<CadastroUsuarioDto>> response = new Response<Page<CadastroUsuarioDto>>();

		PageRequest pageRequest = PageRequest.of(pag, this.qtdPorPagina, Direction.valueOf("ASC"), "nome");
		Page<Usuario> usuarios = this.usuarioService.buscarPorPerfilStatus(PerfilEnum.ROLE_USUARIO, StatusUsuarioEnum.A, pageRequest);
		Page<CadastroUsuarioDto> usuariosDto = usuarios.map(usuario -> this.converterCadastroUsuarioDto(usuario));

		response.setData(usuariosDto);
		return ResponseEntity.ok(response);
	}
	
}
