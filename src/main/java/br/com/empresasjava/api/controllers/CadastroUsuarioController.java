package br.com.empresasjava.api.controllers;

import java.security.NoSuchAlgorithmException;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.empresasjava.api.dto.CadastroUsuarioDto;
import br.com.empresasjava.api.entities.Usuario;
import br.com.empresasjava.api.response.Response;
import br.com.empresasjava.api.services.UsuarioService;
import br.com.empresasjava.api.utils.PasswordUtils;

@RestController
@RequestMapping("/api/cadastrar-usuario")
@CrossOrigin(origins = "127.0.0.1")
public class CadastroUsuarioController {
	
	private static final Logger log = LoggerFactory.getLogger(CadastroUsuarioController.class);

	@Autowired
	private UsuarioService usuarioService;
	
	/**
	 * Cadastra um usuário.
	 * 
	 * @param cadastroUsuarioDto
	 * @param result
	 * @return ResponseEntity<Response<CadastroUsuarioDto>>
	 * @throws NoSuchAlgorithmException
	 */
	@PostMapping
	public ResponseEntity<Response<CadastroUsuarioDto>> cadastrar(@Valid @RequestBody CadastroUsuarioDto cadastroUsuarioDto, BindingResult result) throws NoSuchAlgorithmException {
		log.info("Cadastrando Usuário: {}", cadastroUsuarioDto.toString());
		Response<CadastroUsuarioDto> response = new Response<CadastroUsuarioDto>();

		validarDadosExistentes(cadastroUsuarioDto, result);

		if (result.hasErrors()) {
			log.error("Erro validando dados de cadastro usuário: {}", result.getAllErrors());
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.badRequest().body(response);
		}

		Usuario usuario = this.converterDtoParaUsuario(cadastroUsuarioDto);
		this.usuarioService.persistir(usuario);

		response.setData(this.converterCadastroUsuarioDto(usuario));
		return ResponseEntity.ok(response);
	}

	/**
	 * Verifica se o usuário já foi cadastrado.
	 * 
	 * @param cadastroUsuarioDto
	 * @param result
	 */
	private void validarDadosExistentes(CadastroUsuarioDto cadastroUsuarioDto, BindingResult result) {
		this.usuarioService.usuarioPorLogin(cadastroUsuarioDto.getLogin())
				.ifPresent(emp -> result.addError(new ObjectError("usuario", "Usuário já existente.")));
	}
	
	/**
	 * Converte os dados do DTO para usuário.
	 * 
	 * @param cadastroPJDto
	 * @return Usuario
	 */
	private Usuario converterDtoParaUsuario(CadastroUsuarioDto cadastroUsuarioDto) {
		Usuario usuario = new Usuario();
		usuario.setLogin(cadastroUsuarioDto.getLogin());
		usuario.setNome(cadastroUsuarioDto.getNome());
		usuario.setPerfil(cadastroUsuarioDto.getPerfil());
		usuario.setSenha(PasswordUtils.gerarBCrypt(cadastroUsuarioDto.getSenha()));
		usuario.setStatus(cadastroUsuarioDto.getStatus());
		usuario.setUuid(cadastroUsuarioDto.getUuid());

		return usuario;
	}
	
	/**
	 * Popula o DTO de cadastro com os dados do usuário.
	 * 
	 * @param usuario
	 * @return UsuarioPJDto
	 */
	private CadastroUsuarioDto converterCadastroUsuarioDto(Usuario usuario) {
		CadastroUsuarioDto dto = new CadastroUsuarioDto();
		dto.setUuid(usuario.getUuid());
		dto.setNome(usuario.getNome());
		dto.setPerfil(usuario.getPerfil());
		dto.setStatus(usuario.getStatus());
		dto.setLogin(usuario.getLogin());
		
		return dto;
	}
	
}
