package br.com.empresasjava.api.controllers;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.empresasjava.api.dto.CadastroAtorDto;
import br.com.empresasjava.api.dto.CadastroFilmeDto;
import br.com.empresasjava.api.dto.CadastroVotoDto;
import br.com.empresasjava.api.entities.Ator;
import br.com.empresasjava.api.entities.Filme;
import br.com.empresasjava.api.response.Response;
import br.com.empresasjava.api.services.FilmeService;

@RestController
@RequestMapping("/api/filme")
@CrossOrigin(origins = "127.0.0.1")
public class FilmeController {
	
	private static final Logger log = LoggerFactory.getLogger(FilmeController.class);

	@Autowired
	private FilmeService filmeService;
	
	@Value("${paginacao.qtd_por_pagina}")
	private int qtdPorPagina;
	
	/**
	 * Cadastra um filme.
	 * 
	 * @param CadastroFilmeDto
	 * @param result
	 * @return ResponseEntity<Response<CadastroPJDto>>
	 * @throws NoSuchAlgorithmException
	 */
	@PostMapping
	@PreAuthorize("hasAnyRole('ADMIN')")
	public ResponseEntity<Response<CadastroFilmeDto>> cadastrar(@Valid @RequestBody CadastroFilmeDto cadastroFilmeDto, BindingResult result) throws NoSuchAlgorithmException {
		log.info("Cadastrando Filme: {}", cadastroFilmeDto.toString());
		Response<CadastroFilmeDto> response = new Response<CadastroFilmeDto>();

		if (result.hasErrors()) {
			log.error("Erro validando dados de cadastro filme: {}", result.getAllErrors());
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.badRequest().body(response);
		}
		
		Filme filme = this.converterCadastroFilmeDtoParaFilme(cadastroFilmeDto);

		filme = this.filmeService.persistir(filme);
		
		response.setData(this.converterCadastroFilmeDto(filme));
		return ResponseEntity.ok(response);
	}
	
	/**
	 * Cadastra um voto.
	 * 
	 * @param cadastroPJDto
	 * @param result
	 * @return ResponseEntity<Response<CadastroPJDto>>
	 * @throws NoSuchAlgorithmException
	 */
	@PostMapping(value = "/votar/")
	public ResponseEntity<Response<CadastroVotoDto>> cadastrarVoto(@Valid @RequestBody CadastroVotoDto cadastroVotoDto, BindingResult result) throws NoSuchAlgorithmException {
		log.info("Cadastrando Voto: {}", cadastroVotoDto.toString());
		Response<CadastroVotoDto> response = new Response<CadastroVotoDto>();

		if (result.hasErrors()) {
			log.error("Erro validando dados de cadastro de voto: {}", result.getAllErrors());
			result.getAllErrors().forEach(error -> response.getErrors().add(error.getDefaultMessage()));
			return ResponseEntity.badRequest().body(response);
		}

		Optional<Filme> filme = this.filmeService.filmePorId(cadastroVotoDto.getUuid());
		
		if(!filme.isPresent()) {
			log.error("Filme não encontrado: {}", cadastroVotoDto.getUuid());
			return ResponseEntity.notFound().build();
		}
		
		Filme filmeObj = filme.get();
		filmeObj.setVotoQuantidade(filmeObj.getVotoQuantidade()+1);
		filmeObj.setVotoTotal(filmeObj.getVotoTotal() + cadastroVotoDto.getVoto());
		double media = getMediaFilme(filmeObj.getVotoTotal(), filmeObj.getVotoQuantidade());
		filmeObj.setVotoMedia(media);
		
		filmeService.persistir(filmeObj);
		
		response.setData(cadastroVotoDto);
		return ResponseEntity.ok(response);
	}

	/**
	 * Função que calcula a média de votos de um filme.
	 * @param votoTotal
	 * @param quantidadeVotos
	 * @return double
	 */
	private double getMediaFilme(int votoTotal, int quantidadeVotos) {
		double media = ((double) votoTotal) / quantidadeVotos;
		media = Math.round(media * 100d) / 100d;
		return media;
	}
	
	/**
	 * Consulta um filme.
	 * 
	 * @param id
	 * @return ResponseEntity<Response<Filme>>
	 */
	@GetMapping(value = "/{id}")
	public ResponseEntity<Response<CadastroFilmeDto>> consultar(@PathVariable("id") UUID id) {
		log.info("Consultando um filme: {}", id);
		Response<CadastroFilmeDto> response = new Response<CadastroFilmeDto>();
		Optional<Filme> filme = this.filmeService.filmePorId(id);

		if (!filme.isPresent()) {
			log.error("Erro validando dados de cadastro filme: {}", id);
			return ResponseEntity.notFound().build();
		}
		
		response.setData(this.converterCadastroFilmeDto(filme.get()));
		return ResponseEntity.ok(response);
	}
	
	/**
	 * Popula o DTO de cadastro com os dados do filme.
	 * 
	 * @param filme
	 * @return UsuarioPJDto
	 */
	private CadastroFilmeDto converterCadastroFilmeDto(Filme filme) {
		CadastroFilmeDto dto = new CadastroFilmeDto();
		dto.setUuid(filme.getUuid());
		dto.setNome(filme.getNome());
		dto.setGenero(filme.getGenero());
		dto.setNomeDiretor(filme.getNomeDiretor());
		dto.setSinopse(filme.getSinopse());
		dto.setAtores(convertListaAtorParaAtorDto(filme.getAtores()));
		dto.setVotoMedia(filme.getVotoMedia());
		return dto;
	}
	
	/**
	 * Converte uma lista de Ator para AtoresDto.
	 * @param atoresDto
	 * @return List<Ator>
	 */
	private List<CadastroAtorDto> convertListaAtorParaAtorDto(List<Ator> atores){
		List<CadastroAtorDto> atoresDto = new ArrayList<>();
		for(int i = 0; i < atores.size(); i++) {
			Ator ator = atores.get(i);
			CadastroAtorDto atorDto = new CadastroAtorDto();
			atorDto.setNome(ator.getNome());
			atorDto.setUuid(ator.getUuid());
			atoresDto.add(atorDto);
		}
		return atoresDto;
	}
	
	/**
	 * Popula o DTO de cadastro com os dados do filme.
	 * 
	 * @param filme
	 * @return UsuarioPJDto
	 */
	private Filme converterCadastroFilmeDtoParaFilme(CadastroFilmeDto dto) {
		Filme filme = new Filme();
		filme.setUuid(dto.getUuid());
		filme.setNome(dto.getNome());
		filme.setGenero(dto.getGenero());
		filme.setNomeDiretor(dto.getNomeDiretor());
		filme.setSinopse(dto.getSinopse());
		filme.setAtores(convertListaAtorDtoParaAtor(dto.getAtores(), filme));
		return filme;
	}
	
	/**
	 * Converte uma lista de AtoresDto para Ator.
	 * @param atoresDto
	 * @return List<Ator>
	 */
	private List<Ator> convertListaAtorDtoParaAtor(List<CadastroAtorDto> atoresDto, Filme filme){
		List<Ator> atores = new ArrayList<>();
		for(int i = 0; i < atoresDto.size(); i++) {
			CadastroAtorDto dto = atoresDto.get(i);
			Ator ator = new Ator();
			ator.setNome(dto.getNome());
			ator.setUuid(dto.getUuid());
			ator.setFilme(filme);
			atores.add(ator);
		}
		return atores;
	}
	
	/**
	 * Retorna a listagem de filmes.
	 * 
	 * @return ResponseEntity<Response<CadastroUsuarioDto>>
	 */
	@GetMapping(value = "/listagem")
	public ResponseEntity<Response<Page<CadastroFilmeDto>>> listarFilmes(
			@RequestParam(value = "pag", defaultValue = "0") int pag,
			@RequestParam(value = "diretor", defaultValue = "") String diretor, 
			@RequestParam(value = "nome", defaultValue = "") String nome,
			@RequestParam(value = "genero", defaultValue = "") String genero,
			@RequestParam(value = "ator", defaultValue = "") String ator) {
		log.info("Buscando filmes na página: {}", pag);
		Response<Page<CadastroFilmeDto>> response = new Response<Page<CadastroFilmeDto>>();

		PageRequest pageRequest = PageRequest.of(pag, this.qtdPorPagina, Direction.valueOf("ASC"), "votoQuantidade", "nome");
		Collection<String> atores = getListaAtores(ator);
		Page<Filme> filmes = this.filmeService.buscarPorDiretorNomeGeneroAtor(diretor, nome, genero, atores, pageRequest);
		Page<CadastroFilmeDto> filmesDto = filmes.map(filme -> this.converterCadastroFilmeDto(filme));

		response.setData(filmesDto);
		return ResponseEntity.ok(response);
	}
	
	/**
	 * Função que converte uma string com os nomes dos atores em uma lista com esses nomes.
	 * @param ator
	 * @return Collection<String>
	 */
	private Collection<String> getListaAtores(String ator){
		Collection<String> lista = new  ArrayList<String>();
		String[] atoresArr = ator.split(",");
		for(String atorStr : atoresArr) {
			lista.add(atorStr.trim());
		}
		return lista;
	}
	
}
