package br.com.empresasjava.api.services;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import br.com.empresasjava.api.entities.Usuario;
import br.com.empresasjava.api.enums.PerfilEnum;
import br.com.empresasjava.api.enums.StatusUsuarioEnum;

public interface UsuarioService {
	
	/**
	 * Retorna o usuário consultado pelo id.
	 * 
	 * @param uuid
	 * @return Usuario
	 */
	Optional<Usuario> usuarioPorId(UUID uuid);
	
	/**
	 * Retorna o usuário consultado pelo login.
	 * 
	 * @param login
	 * @return Usuario
	 */
	Optional<Usuario> usuarioPorLogin(String login);
	
	/**
	 * Retorna o usuário que foi persistido.
	 * 
	 * @param usuario
	 * @return Usuario
	 */
	Usuario persistir(Usuario usuario);
	
	/**
	 * Retorna uma lista paginada de usuários filtrados pelo perfil e status.
	 * 
	 * @param perfil
	 * @Param status
	 * @param pageRequest
	 * @return Page<Usuario>
	 */
	Page<Usuario> buscarPorPerfilStatus(PerfilEnum perfil, StatusUsuarioEnum status, PageRequest pageRequest);
}
