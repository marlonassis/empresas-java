package br.com.empresasjava.api.services;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import br.com.empresasjava.api.entities.Filme;

public interface FilmeService {
	
	/**
	 * Retorna o filme consultado pelo id.
	 * 
	 * @param uuid
	 * @return Filme
	 */
	Optional<Filme> filmePorId(UUID uuid);
	
	/**
	 * Retorna o filme que foi persistido.
	 * 
	 * @param filme
	 * @return Filme
	 */
	Filme persistir(Filme filme);
	
	/**
	 * Retorna uma lista paginada de filmes filtrados pelo diretor, nome ou gênero.
	 * 
	 * @param diretor
	 * @Param nome
	 * @Param gênero
	 * @Param ator
	 * @param pageRequest
	 * @return Page<Filme>
	 */
	Page<Filme> buscarPorDiretorNomeGeneroAtor(String diretor, String nome, String genero, Collection<String> atores, PageRequest pageRequest);
}
