package br.com.empresasjava.api.services.impl;

import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import br.com.empresasjava.api.entities.Usuario;
import br.com.empresasjava.api.enums.PerfilEnum;
import br.com.empresasjava.api.enums.StatusUsuarioEnum;
import br.com.empresasjava.api.repositories.UsuarioRepository;
import br.com.empresasjava.api.services.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService {

	private static final Logger log = LoggerFactory.getLogger(UsuarioServiceImpl.class);
	
	@Autowired
	private UsuarioRepository usuarioRepository;

	@Override
	public Optional<Usuario> usuarioPorId(UUID uuid) {
		log.info("Buscando usuário pelo id.");
		return usuarioRepository.findById(uuid);
	}
	
	@Override
	public Optional<Usuario> usuarioPorLogin(String login) {
		log.info("Buscando usuário pelo login.");
		return usuarioRepository.findByLogin(login);
	}

	@Override
	public Usuario persistir(Usuario usuario) {
		log.info("Persistindo usuário {}.", usuario);
		try {
			return usuarioRepository.save(usuario);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Page<Usuario> buscarPorPerfilStatus(PerfilEnum perfil, StatusUsuarioEnum status, PageRequest pageRequest) {
		log.info("Buscando usuários para o perfil {} e status {}", perfil, status);
		return this.usuarioRepository.findByPerfilAndStatus(perfil, status, pageRequest);
	}
	
}
