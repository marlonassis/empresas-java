package br.com.empresasjava.api.services.impl;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import br.com.empresasjava.api.entities.Filme;
import br.com.empresasjava.api.repositories.FilmeRepository;
import br.com.empresasjava.api.services.FilmeService;

@Service
public class FilmeServiceImpl implements FilmeService {

private static final Logger log = LoggerFactory.getLogger(FilmeServiceImpl.class);
	
	@Autowired
	private FilmeRepository filmeRepository;
	
	@Override
	public Optional<Filme> filmePorId(UUID uuid) {
		log.info("Buscando filme pelo id.");
		return filmeRepository.findById(uuid);
	}

	@Override
	public Filme persistir(Filme filme) {
		log.info("Persistindo filme {}.", filme);
		try {
			return filmeRepository.save(filme);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public Page<Filme> buscarPorDiretorNomeGeneroAtor(String diretor, String nome, 
												  String genero, Collection<String> atores, PageRequest pageRequest) {
		log.info("Buscando filmes para o diretor {}, nome {} ou gênero {}", diretor, nome, genero);
		return this.filmeRepository.findByNomeDiretorOrNomeOrGeneroOrAtoresNomeIn(diretor, nome, genero, atores, pageRequest);
	}

}
