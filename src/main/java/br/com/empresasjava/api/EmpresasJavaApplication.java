package br.com.empresasjava.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EmpresasJavaApplication {

	public static void main(String[] args) {
		SpringApplication.run(EmpresasJavaApplication.class, args);
	}

}
