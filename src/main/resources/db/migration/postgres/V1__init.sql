CREATE TABLE public.tb_filme
(
    uuid_filme uuid NOT NULL,
    cv_nome character varying NOT NULL,
    cv_nome_diretor character varying NOT NULL,
    cv_sinopse character varying NOT NULL,
    fl_voto_media double precision NOT NULL,
    in_voto_total integer NOT NULL,
    in_voto_quantidade integer NOT NULL,
    CONSTRAINT tb_filme_pk_uuid_filme PRIMARY KEY (uuid_filme)
);

COMMENT ON TABLE public.tb_filme
    IS 'Registra os filmes que ja foram lancados.';

CREATE TABLE public.tb_ator
(
    uuid_ator uuid NOT NULL,
    cv_nome character varying NOT NULL,
    uuid_filme uuid NOT NULL,
    CONSTRAINT tb_ator_pk_uuid_ator PRIMARY KEY (uuid_ator),
    CONSTRAINT tb_ator_tb_filme_fk_uuid_filme FOREIGN KEY (uuid_filme)
        REFERENCES public.tb_filme (uuid_filme) MATCH SIMPLE
        ON UPDATE CASCADE
        ON DELETE CASCADE
);

COMMENT ON TABLE public.tb_ator
    IS 'Registra cada ator relacionado ao filme.';

CREATE TABLE public.tb_usuario
(
    uuid_usuario uuid NOT NULL,
    cv_nome character varying NOT NULL,
    tp_perfil character varying NOT NULL,
    tp_status character varying NOT NULL,
    cv_login character varying NOT NULL,
    cv_senha character varying NOT NULL,
    CONSTRAINT tb_usuario_pk_uuid_usuario PRIMARY KEY (uuid_usuario)
);

COMMENT ON TABLE public.tb_usuario
    IS 'Registra os usuários que possuem acesso ao sistema.';
