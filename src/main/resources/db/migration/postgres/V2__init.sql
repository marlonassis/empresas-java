INSERT INTO public.tb_usuario(
	uuid_usuario, cv_nome, tp_perfil, tp_status, cv_login, cv_senha)
	VALUES ('4f09d0f1-d90d-469b-a473-0d4194ce466b', 'Jorge da Silva 123', 'ADM', 'A', 'jorge', '$2a$10$SstmC5eqqcQwgbX8Noxl4.MBUX32MpJc/4NMB0CVHm/GwX4yIa.jq');
	
INSERT INTO public.tb_usuario(
	uuid_usuario, cv_nome, tp_perfil, tp_status, cv_login, cv_senha)
	VALUES ('ee2f3fb0-f17a-4aea-b602-e2a08272a458', 'Carlos Almeida 456', 'USU', 'A', 'carlos', '$2a$10$255zGtknLyPxAXuIdHPnie.CQWhhpDkVYC9rOIMinqRtYxqA28VdS');

INSERT INTO public.tb_usuario(
	uuid_usuario, cv_nome, tp_perfil, tp_status, cv_login, cv_senha)
	VALUES ('6d21d80f-e7f3-4797-866d-fd8fef6a3d27', 'Aline Santos 789', 'USU', 'I', 'aline', '$2a$10$LQUbSYUl4V9r26.pkcU/sen9tLK3V29vn/jt/g24F1u1XmnEFvXMu');